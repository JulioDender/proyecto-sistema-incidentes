<?php

namespace App\Repository;

use App\Entity\Incidente;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @extends ServiceEntityRepository<Incidente>
 *
 * @method Incidente|null find($id, $lockMode = null, $lockVersion = null)
 * @method Incidente|null findOneBy(array $criteria, array $orderBy = null)
 * @method Incidente[]    findAll()
 * @method Incidente[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IncidenteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Incidente::class);
    }

    public function save(Incidente $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Incidente $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function getIncidenteResidente($idUser): ?array
    {
        //se hace un left join para obtener las incidentes aun cuando no tengan registrado un tecnico Id
        $strSql = "SELECT incidentes.id,
                   incidentes.fecha,
                   incidentes.descripcion,
                   incidentes.estado,
                   tipoIncidente.descripcion descripciontipoIncidente,
                   incidentes.total,
                   userGuardia.nombres nombre_guardia,
                   userGuardia.apellidos apellido_guardia
                   FROM App\Entity\Incidente incidentes
                   LEFT JOIN App\Entity\Usuario userGuardia
                   WITH incidentes.guardia_id = userGuardia.id
                   LEFT JOIN App\Entity\TipoIncidente tipoIncidente
                   WITH incidentes.tipo_id = tipoIncidente.id
                   WHERE incidentes.residente_id = :residente AND incidentes.estado_base = :estado ";
        return $this->_em->createQuery($strSql)
                    ->setParameter('residente',$idUser)
                    ->setParameter('estado','A')
                    ->getResult();         
    }
    public function getIncidenteGuardia(): ?array
    {
        //se hace un left join para obtener las incidentes aun cuando no tengan registrado un tecnico Id
        $strSql = "SELECT incidentes.id,
                   incidentes.fecha,
                   incidentes.descripcion,
                   incidentes.estado,
                   tipoIncidente.descripcion descripciontipoIncidente,
                   incidentes.solucion,
                   userGuardia.nombres nombre_guardia,
                   userGuardia.apellidos apellido_guardia,
                   userResidente.nombres residente_nombres,
                   userResidente.apellidos residente_apellidos
                   FROM App\Entity\Incidente incidentes
                   JOIN App\Entity\Usuario userResidente
                   WITH incidentes.residente_id = userResidente.id
                   LEFT JOIN App\Entity\Usuario userGuardia
                   WITH incidentes.guardia_id = userGuardia.id
                   LEFT JOIN App\Entity\TipoIncidente tipoIncidente
                   WITH incidentes.tipo_id = tipoIncidente.id
                   WHERE incidentes.estado =:estado AND incidentes.estado_base = :estado_b ";
        return $this->_em->createQuery($strSql)
        ->setParameter('estado','En Espera')
                    ->setParameter('estado_b','A')
                    ->getResult();         
    }
    public function getIncidenteGerente(): ?array
    {
        //se hace un left join para obtener las incidentes aun cuando no tengan registrado un tecnico Id
        $strSql = "SELECT incidentes.id,
                   incidentes.fecha,
                   incidentes.descripcion,
                   incidentes.estado,
                   tipoIncidente.descripcion descripciontipoIncidente,
                   incidentes.solucion,
                   incidentes.total,
                   incidentes.observacion,
                   userGuardia.nombres nombre_guardia,
                   userGuardia.apellidos apellido_guardia,
                   userResidente.nombres residente_nombres,
                   userResidente.apellidos residente_apellidos
                   FROM App\Entity\Incidente incidentes
                   JOIN App\Entity\Usuario userResidente
                   WITH incidentes.residente_id = userResidente.id
                   LEFT JOIN App\Entity\Usuario userGuardia
                   WITH incidentes.guardia_id = userGuardia.id
                   LEFT JOIN App\Entity\TipoIncidente tipoIncidente
                   WITH incidentes.tipo_id = tipoIncidente.id
                   WHERE incidentes.estado =:estado AND incidentes.estado_base = :estado_b ";
        return $this->_em->createQuery($strSql)
                    ->setParameter('estado','Atendido')
                    ->setParameter('estado_b',"A")
                    ->getResult();         
    }
    


//    /**
//     * @return Incidente[] Returns an array of Incidente objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('i')
//            ->andWhere('i.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('i.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Incidente
//    {
//        return $this->createQueryBuilder('i')
//            ->andWhere('i.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}