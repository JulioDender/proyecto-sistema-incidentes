<?php

namespace App\Entity;

use App\Repository\IncidenteRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: IncidenteRepository::class)]
class Incidente
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $fecha = null;

    #[ORM\Column(length: 100)]
    private ?string $descripcion = null;

    #[ORM\Column]
    private ?int $tipo_id = null;

    #[ORM\Column(length: 100, nullable: true)]
    private ?string $solucion = null;

    #[ORM\Column(length: 50)]
    private ?string $estado = null;

    #[ORM\Column(length: 1, nullable: true)]
    private ?string $estado_base = null;

    #[ORM\Column]
    private ?int $residente_id = null;

    #[ORM\Column(nullable: true)]
    private ?int $guardia_id = null;

    #[ORM\Column(nullable: true)]
    private ?int $total = null;

    #[ORM\Column(length: 100, nullable: true)]
    private ?string $observacion = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getTipoId(): ?int
    {
        return $this->tipo_id;
    }

    public function setTipoId(int $tipo_id): self
    {
        $this->tipo_id = $tipo_id;

        return $this;
    }

    public function getSolucion(): ?string
    {
        return $this->solucion;
    }

    public function setSolucion(?string $solucion): self
    {
        $this->solucion = $solucion;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(string $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getEstadoBase(): ?string
    {
        return $this->estado_base;
    }

    public function setEstadoBase(?string $estado_base): self
    {
        $this->estado_base = $estado_base;

        return $this;
    }

    public function getResidenteId(): ?int
    {
        return $this->residente_id;
    }

    public function setResidenteId(int $residente_id): self
    {
        $this->residente_id = $residente_id;

        return $this;
    }

    public function getGuardiaId(): ?int
    {
        return $this->guardia_id;
    }

    public function setGuardiaId(?int $guardia_id): self
    {
        $this->guardia_id = $guardia_id;

        return $this;
    }

    public function getTotal(): ?int
    {
        return $this->total;
    }

    public function setTotal(?int $total): self
    {
        $this->total = $total;

        return $this;
    }

    public function getObservacion(): ?string
    {
        return $this->observacion;
    }

    public function setObservacion(?string $observacion): self
    {
        $this->observacion = $observacion;

        return $this;
    }
}
