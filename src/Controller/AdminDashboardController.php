<?php

namespace App\Controller;

use App\Entity\Usuario;
use App\Entity\Incidente;
use App\Repository\UsuarioRepository;
use App\Repository\IncidenteRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Repository\TipoIncidenteRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Form\RegisterIncidenteType;

class AdminDashboardController extends AbstractController
{
    #[Route('/admin/dashboard', name: 'app_admin_dashboard')]
    public function index(Request $request,IncidenteRepository $incidenteRepository ,UsuarioRepository $usuarioRepository): Response
    {
        $email = $request->getSession()->get('_security.last_username', '');
        
        $usuario = $usuarioRepository->findOneBy(["email"=>$email]);
        return $this->render('admin_dashboard/index.html.twig', [
            'listIncidentes'=>$incidenteRepository->getIncidenteGerente(),
        ]);
    }

    #[Route('/admin/dashboard/atender/{id}', name: 'app_admin_dashboard_atender')]
    public function atender(Request $request,TipoIncidenteRepository $tipoIncidenteRepository,Incidente $incidente,IncidenteRepository $incidenteRepository,ManagerRegistry $doctrine, UsuarioRepository $usuarioRepository): Response
    {
        
        
        $email = $request->getSession()->get('_security.last_username', '');
        $arrayTipos = $tipoIncidenteRepository->findAll();
        $objUsuario = $usuarioRepository->findOneBy(["email"=>$email]);
        $form = $this->createForm(RegisterIncidenteType::class, $incidente, ['accion'=>'editGerente','arrayTipos'=>$arrayTipos]);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $incidente=$form->getData();

           // $incidente->setEstado("En Espera");
            $this->addFlash("success", "Exitos: El incidente fue atendido");
            $incidenteRepository->save($incidente,true);
 
          return $this->redirectToRoute('app_guardia_dashboard');  
        }
        return $this->render('admin_dashboard/editarIncidente.html.twig', [
            'formulario'=>$form->createView(),
        ]);
        
    }
}

