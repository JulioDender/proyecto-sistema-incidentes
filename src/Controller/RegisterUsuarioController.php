<?php

namespace App\Controller;

use App\Entity\Usuario;
use App\Form\RegisterUserType;
use App\Repository\UsuarioRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class RegisterUsuarioController extends AbstractController
{
    #[Route('/register/usuario', name: 'app_register_usuario')]
    public function index(Request $request, UserPasswordHasherInterface $encoderPassword, ManagerRegistry $doctrine, UsuarioRepository $userRepository): Response
    {
        $user = new Usuario(USUARIO ::ROLE_USER ,"Tester");
        $form = $this -> createForm(RegisterUserType :: class, $user);
        $form -> handleRequest($request);
        if($form ->isSubmitted() && $form ->isValid())
        {
            $user=$form ->getData();
            $user -> setPassword($encoderPassword ->hashPassword($user,$form["password"]->getData()));
            /*$user->setClave(
                $encoderPassword->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );*/
            if ($user->getTipo() == "Admin")
            {
                $user -> setRoles([USUARIO ::ROLE_ADMIN]);
            }
            elseif ($user->getTipo() == "Guardia" )
            {
                $user -> setRoles([USUARIO ::ROLE_GUARDIA]);
            }
            elseif ($user->getTipo() == "Residente" )
            {
                $user -> setRoles([USUARIO ::ROLE_RESIDENTE]);
            }
            
            $userRepository->save($user,true);    
            return $this-> redirectToRoute('app_login');
                
            
        }
        return $this->render('register_usuario/index.html.twig', [
            'formulario' => $form ->createView(),
        ]);
    }
}
