<?php

namespace App\Form;

use App\Entity\Incidente;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class RegisterIncidenteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        if($options['accion']=='crearIncidente')
        {
            $builder
            ->add('fecha', DateTimeType::class,["widget"=>"single_text"])
            ->add('descripcion',TextType::class)
            ->add('tipo_id',ChoiceType:: class, array(
                'choices' => array(
                    'Incendio' => 1,
                    'Robo' => 2,
                    'Accidente automovilisto' => 3,
                    'Daño a la propiedad privada'=>4,
                    'Daño de areas sociales'=>5) 
                ));
        }

           /* ->add('tipo_id',ChoiceType::class,
            ['choices'=>$options["arrayTipos"],//array que se envia desde el controlador
             'choice_value'=>'id',//atributo de valor que se setea al seleccionar una opcion
             'choice_label'=>'descripcion',//lo que se mostrara en pantala en las opciones
             'label'=>'Tipo de Incidente',//lo que se muestra en pantalla por defecto
             'mapped'=>false]);*/
             if ($options['accion']=='editGuardia')
             {
                $builder
                ->add('fecha', DateTimeType::class,["widget"=>"single_text","disabled"=>true])
                ->add('descripcion',TextType::class,["disabled"=>true])
                 ->add('tipo_id',ChoiceType:: class, array(
                     'choices' => array(
                    'Incendio' => 1,
                    'Robo' => 2,
                    'Accidente automovilisto' => 3,
                    'Daño a la propiedad privada'=>4,
                    'Daño de areas sociales'=>5),
                    'disabled'=> true 
                     ))
                ->add('solucion',TextType::class)
                ->add('estado',ChoiceType:: class, array(
                    'choices' => array(
                        'En espera' => 'En espera',
                        'Atendido' => 'Atendido') 
                    ));
             }
             if ($options['accion']=='editGerente')
             {
                $builder
                ->add('fecha', DateTimeType::class,["widget"=>"single_text","disabled"=>true])
                ->add('descripcion',TextType::class,["disabled"=>true])
                 ->add('tipo_id',ChoiceType:: class, array(
                    "disabled"=>true,
                    'choices' => array(
                    'Incendio' => 1,
                    'Robo' => 2,
                    'Accidente automovilisto' => 3,
                    'Daño a la propiedad privada'=>4,
                    'Daño de areas sociales'=>5) 
                     ))
                ->add('solucion',TextType::class,["disabled"=>true])
                ->add('total', NumberType::class)
                ->add('observacion', TextType::class);
            
        
             }
        $builder->add('save', SubmitType :: class, ['label' => 'Registrar']);
          
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Incidente::class,
            'accion' => 'crearIncidente',
            'arrayTipos'=>array()
        ]);
    }
}
